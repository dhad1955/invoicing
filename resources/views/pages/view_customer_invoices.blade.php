@extends('template.page')


@section('pagecontent')

    <h3>Viewing invoices for {{$customer->company_name}}</h3>

    <a class="btn btn-primary" href="/invoice/create/{{$customer->id}}"><i class="fa fa-plus"></i> Create Invoice</a>

    @if($customer->invoices->count() > 0)
        <table class="table" style="width: 100%">
            <thead>
            <th>Invoice ID</th>
            <th>Value</th>
            <th>Date</th>
            <th>Delete</th>
            <th>Edit</th>
            </thead>

            <tbody>
            @foreach($customer->invoices as $invoice)
                <tr>
                    <td>{{$invoice->id}}</td>
                    <td>&pound;{{friendly_money($invoice->getGrandTotal())}}</td>
                    <td>{{$invoice->created_at}}</td>
                    <td><a class="btn btn-danger" href="/invoice/delete/{{$invoice->id}}">Delete</a></td>
                    <td><a class="btn btn-primary" href="/invoice/edit/{{$invoice->id}}">Edit</a></td>
                </tr>
            @endforeach

            </tbody>
        </table>

    @endif
@stop
