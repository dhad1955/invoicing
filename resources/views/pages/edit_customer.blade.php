@extends('template.page')

@section('pagecontent')



    @include('template.customer_form')

    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary" href="/customer/invoices/{{$model->id}}">Manage Invoices</a>
            <Br/>
        </div>
    </div>
@stop

