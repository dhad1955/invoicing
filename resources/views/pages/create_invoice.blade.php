@extends('template.page')


@section('pagecontent')

    <h3>Create a new invoice</h3>

    <invoice-editor custid="{{$customer->id}}" action="new">

    </invoice-editor>

    <style>
        .height {
            min-height: 200px;
        }

        .icon {
            font-size: 47px;
            color: #5CB85C;
        }

        .iconbig {
            font-size: 77px;
            color: #5CB85C;
        }

        .table > tbody > tr > .emptyrow {
            border-top: none;
        }

        .table > thead > tr > .emptyrow {
            border-bottom: none;
        }

        .table > tbody > tr > .highrow {
            border-top: 3px solid;
        }

         td {
            text-align: left;
        }
    </style>

@stop
