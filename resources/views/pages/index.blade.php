@extends('template.page')

@section('pagecontent')

    <div class="row" id="index">

        <div class="col-lg-12">

            <div class="row">
                <div class="col-lg-12">
                    <a href="/customers"><i class="fa fa-users"></i> Manage customers</a>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <a href="/customer/add"><i class="fa fa-user-plus"></i> New Customer</a>
                </div>
            </div>


        </div>


    </div>


@stop
