<ul class="breadcrumb">
    <li><a href="/" class="ext">home</a></li>
    @foreach(breadcrumbs()->all() as $key => $breadcrumb)
        <li><a href="{{$breadcrumb['url']}}">{{(strtolower(strip_tags($breadcrumb['name'])))}} </a></li>
    @endforeach
    <li><a class="active" href="javascript:void(0)">{{(strtolower(strip_tags(breadcrumbs()->getLastName())))}}</a></li>
</ul>