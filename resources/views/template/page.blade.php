<head>
    <title>Invoice task - {{$title or ''}}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          type="text/css"/>
    <link rel="stylesheet" href="/css/style.css" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css"/>
</head>

<body id="body">

<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h1>Invoice system</h1>
        </div>
    </div>

   <div class="row">
       <div class="col-md-12">
           @include('template.breadcrumbs')
       </div>
   </div>


    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    @yield('pagecontent')
</div>

</body>
<script src="/js/main.js" type="text/javascript"></script>
