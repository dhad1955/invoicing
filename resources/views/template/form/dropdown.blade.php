

<select name="{{$key}}" class="form-control">

    @foreach($items as $item)

        @if(old($key) == ($item->{$value_key}) || ($model != null && $model[$key] == ($item->{$value_key})))
            <option selected="selected" value="{{($item->{$value_key})}}">{{($item->{$name_key})}}</option>
        @else
            <option value="{{($item->{$value_key})}}">{{($item->{$name_key})}}</option>

        @endif

    @endforeach

</select>