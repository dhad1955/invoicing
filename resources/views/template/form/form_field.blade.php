@include('template.form.'.$type)

@if($errors->default->has($key))
    <span class="form-error">{{$errors->default->first($key)}}</span>
@endif
