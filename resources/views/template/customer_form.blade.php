<form action="{{$action}}" method="POST">

    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-6">

            <div class="form-group">

                <label>Customer name*</label>

                {!! input('text', 'name', $model, ['placeholder' => 'Please enter the customer name']) !!}


            </div>
        </div>

        <div class="col-md-6">


            <div class="form-group">

                <label for="inputPassword">Company name*</label>

                {!! input('text', 'company_name', $model, ['placeholder' => 'Please enter the company name']) !!}


            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-6">

            <div class="form-group">

                <label>Telephone number*</label>

                {!! input('text', 'tel_no', $model, ['placeholder' => 'Please enter the telephone number']) !!}


            </div>
        </div>

        <div class="col-md-6">


            <div class="form-group">

                <label for="inputPassword">Email address*</label>

                {!! input('text', 'email', $model, ['placeholder' => 'Please enter the email']) !!}


            </div>
        </div>

    </div>

    {{-- Address details --}}

    <div class="row">


        <div class="col-md-6">

            <div class="form-group">

                <label>Address1*</label>

                {!! input('text', 'address1', $model, ['placeholder' => 'Please enter the first line of the address']) !!}


            </div>
        </div>

        <div class="col-md-6">


            <div class="form-group">

                <label for="inputPassword">Address2</label>

                {!! input('text', 'address2', $model, ['placeholder' => 'Please enter the second line of the address']) !!}


            </div>
        </div>

    </div>


    <div class="row">


        <div class="col-md-4">

            <div class="form-group">

                <label>City*</label>

                {!! input('text', 'city', $model, ['placeholder' => 'Please enter the city']) !!}


            </div>
        </div>

        <div class="col-md-4">


            <div class="form-group">

                <label for="inputPassword">Postcode*</label>

                {!! input('text', 'postcode', $model, ['placeholder' => 'Please enter the post code']) !!}


            </div>
        </div>

        <div class="col-md-4">


            <div class="form-group">

                <label for="inputPassword">Country*</label>

                {!! input('dropdown', 'country_id', $model, ['items' => \App\Country::all(), 'value_key' => 'id', 'name_key' => 'name']) !!}


            </div>
        </div>


    </div>


    <div class="col-md-6"></div>
    <div class="col-md-6">
        <div class="pull-right">


            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>


</form>
