

global.Vue = require('Vue');

Vue.use(require('vue-resource'));

import SearchCustomers from './components/SearchCustomers.vue';
import InvoiceEditor from './components/InvoiceEditor.vue';

Vue.component('customer-search', SearchCustomers);

Vue.component('invoice-editor', InvoiceEditor);

global.Core = new Vue({

    
    el: '#body',
    
    ready() {
        
    },


});