<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 14/06/2017
 * Time: 18:07
 */

namespace App;


interface Priceable
{

    
    
    // Get the sub total of this item, without VAT or extras
    public function getSubTotal();
    
    // Get the grand total of this item, with VAT and/or extras
    public function getGrandTotal();
}