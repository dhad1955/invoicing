<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 14/06/2017
 * Time: 18:03
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{


    public $guarded = ['id'];
    
    // Return the invoices related to this customer
    
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
    
    // Return the country for this customer
    
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    
    // Create and associate a new invoice to the customer
    public function createInvoice() 
    {
        return Invoice::create(['customer_id' => $this->getKey()]);
        
    }
    
    public function deleteInvoice($invoice_id)
    {
        
        // security: make sure the invoice belongs to this customer!
        $invoice = Invoice::where('invoice_id', $invoice_id)
            ->where('customer_id', $this->getKey());
        
        if($invoice != null) {
            $invoice->delete();
            return true;
        } else {
            return false;
        }
    }

}