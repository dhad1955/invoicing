<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 14/06/2017
 * Time: 18:52
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    
    public $table = 'countries';

}