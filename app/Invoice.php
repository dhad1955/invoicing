<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 14/06/2017
 * Time: 18:03
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Invoice extends Model implements Priceable
{

    public $guarded = ['id'];

    public $casts =
        ['id' => 'int',
            'customer_id' => 'int'];

    public $appends = ['items'];


    public function getItemsAttribute()
    {
        return $this->invoice_items()->get();
    }

    // Return the assigned customer to this invoice

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    // Return the associated items with this invoice

    public function invoice_items()
    {
        return $this->hasMany(InvoiceItem::class);
    }


    public function getSubTotal()
    {
        $total = 0.00;

        // Add up the invoice items without vat
        foreach ($this->invoice_items as $invoice_item) {
            $total += ($invoice_item->getSubTotal() * $invoice_item->quantity);

        }
        return $total;
    }

    public function getGrandTotal()
    {
        return vat($this->getSubTotal(), $this->customer);
    }


    // add a new item to this invoice

    public function addItem($name, $price, $quantity)
    {
        return InvoiceItem::create(array_merge(['invoice_id' => $this->getKey()], compact('name', 'price', 'quantity')));
    }
}