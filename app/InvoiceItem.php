<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 14/06/2017
 * Time: 18:05
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model implements Priceable
{
    
    public $guarded = ['id'];

    public $casts =
        [
            'id' => 'int',
            'price' => 'float',
            'quantity' => 'float'
        ];
    
    // return the invoice this belongs to
    
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }


    // find the customer for this invoice item
    private function customer() 
    {
        return $this->hasManyThrough(Customer::class, 'invoice');
    }
    

    public function getSubTotal()
    {
        return $this->price;
    }

    public function getGrandTotal()
    {
       return vat($this->price, $this->customer);
    }
    
    
   
}