<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 14/06/2017
 * Time: 23:51
 */

namespace App;
use Illuminate\Support\Facades\Request;

class BreadCrumbs
{
    private $breadcrumbs;
    private $last;

    public function __construct()
    {
        $this->breadcrumbs = collect([]);
    }

    public function all()
    {
        return $this->breadcrumbs->except($this->last);
    }

    public function last()
    {
        return $this->breadcrumbs->get($this->last);
    }

    public function getLastName()
    {
        $last = $this->last();
        return $last['name'];
    }

    public function forget()
    {
        $this->breadcrumbs = collect();
    }

    
    // we need to set a unique key for every different page so it doesn't overwrite !
    
    public function remember($key, $name)
    {
        // Do we already have a breadcrumb? if so remove the tail
        if ($this->breadcrumbs->has($key)) {
            $new_breadcrumbs = collect();

            foreach ($this->breadcrumbs as $new_key => $breadcrumb) {
                $new_breadcrumbs->put($new_key, $breadcrumb);
                if ($key == $new_key) {
                    $this->breadcrumbs = $new_breadcrumbs;
                    break;
                }
            }
        }
        $this->breadcrumbs->put($key, ['name' => $name, 'url' => Request::url()]);
        $this->last = $key;
    }

    public function get($key)
    {
        return $this->breadcrumbs->get($key);
    }
}