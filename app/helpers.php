<?php
// Helper functions


// get the VAT rate from the Config file and apply it to the passed price
if(!function_exists('vat'))
{
    function vat($price, $customer = null)
    {
        
        $vat = $customer == null || $customer->country == null ? config('finance.vat_default') : $customer->country->vat;
        
        return $price * (1 + ($vat / (double)(100)));
    }
}


// Return 

if (!function_exists('breadcrumbs')) {
    function breadcrumbs($key = null, $url = null)
    {
        $breadcrumbs = session('breadcrumbs');
        if ($key == null) {
            return $breadcrumbs;
        }

        if ($url == null) {
            return $breadcrumbs->get($key);
        } else {
            $breadcrumbs->remember($key, $url);
        }
        
    }
}


// Return an input form


if(!function_exists('input')) 
{
    function input($type, $key, $model = null, $attributes = array()) 
    {
        return view('template.form.form_field', array_merge(compact('type', 'key', 'model'), $attributes));
        
    }
}

// format money properly eg £3233.4 to £3,233.40

if(!function_exists('friendly_money')) 
{
    function friendly_money($price)
    {

        if (!is_numeric($price))
            return $price;


        return number_format((float)$price, 2, '.', ',');
    }

}