<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 15/06/2017
 * Time: 12:51
 */

namespace App\Http\Controllers;


use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('breadcrumbs');
    }


    // add a new customer


    public function getAdd()
    {
        breadcrumbs()->forget();
        breadcrumbs('index', 'Add a new customer');
        return view('pages.add_customer', ['action' => '/customer/create', 'model' => []]);
    }


    // edit a customer


    public function getEdit($customer_id)
    {
        $customer = Customer::find($customer_id);

        if($customer == null) {
            die("Invalid customer");
        }

        breadcrumbs('customer', 'Edit customer - '.$customer->id);


        return view('pages.edit_customer', ['action' => '/customer/update/'.$customer->id, 'model' => $customer]);

    }
    
    // return the invoices for a given customer

    public function getInvoices($customer_id) 
    {

        breadcrumbs('edit_invoices', 'Edit customer invoices');

        $customer = Customer::findOrFail($customer_id);
        
        return view('pages.view_customer_invoices', compact('customer'));
        
    }

    // delete an customer with the set id then redirect to the customers search

    public function getDelete($customer_id)
    {
        $customer = Customer::find($customer_id);

        if($customer == null) {
            return redirect('/customers');
        } else {
            $customer->delete();
            return redirect('/customers')
                ->with('status', 'Customer deleted successfully');
        }

    }
    
    // update a customer request


    public function postUpdate($customer_id, Request $request)
    {
        $this->validateCustomerForm($request);

        $customer = Customer::findOrFail($customer_id);

        $customer->update($request->except('_token'));
        return redirect('/customer/edit/'.$customer_id)
            ->with('status', 'Customer details updated successfully');

    }


    // create a new customer request

    public function postCreate(Request $request) {

        $this->validateCustomerForm($request);

        $customer = Customer::create($request->except('_token'));

        return redirect('/customer/edit/'.$customer->id)
            ->with('status', 'New customer created!');

    }


    // Return a list of customers from a search result


    public function postSearch(Request $request)
    {

        $searchTerm = '%'.$request->input('search').'%';

        $results =
            Customer::where('name', 'LIKE', $searchTerm)
            ->orWhere('company_name', 'LIKE', $searchTerm)
            ->orWhere('email', 'LIKE', $searchTerm)->get();

        return ['results' => $results];
    }
    
    
    // Run the validation checks on the customer form


    private function validateCustomerForm(Request $request)
    {

        $this->validate($request,
            [
                'name' => 'required|min:3|max:100',
                'company_name' => 'required|min:3|max:100',
                'tel_no' => 'required|min:3|max:100',
                'email' => 'required|min:3|max:100|email',
                'address1' => 'required|min:3|max:100',
                'city' => 'required|min:3|max:100',
                'postcode' => 'required|min:6|max:10',
            ]);

    }




}