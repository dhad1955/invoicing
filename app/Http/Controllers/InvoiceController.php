<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 15/06/2017
 * Time: 14:42
 */

namespace App\Http\Controllers;


use App\Customer;
use App\Invoice;
use App\InvoiceItem;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('breadcrumbs');
    }

    
    // Create a new invoice

    public function getCreate($customer_id)
    {

        breadcrumbs('invoice', 'Create new invoice');
        $customer = Customer::findOrFail($customer_id);

        return view('pages.create_invoice', compact('customer'));
    }
    
    // Edit an already existing invoice

    public function getEdit($invoice_id)
    {
        breadcrumbs('invoice', 'Edit invoice'.$invoice_id);

        $invoice = Invoice::findOrFail($invoice_id);

        return view('pages.edit_invoice', compact('invoice'));
    }
    
    // API Call: return an invoices data as JSON

    public function getGet($invoice_id)
    {
        return Invoice::find($invoice_id);
    }

    
    // Delete an invoice then redirect back to the invoices page

    public function getDelete($invoice_id) {
        $invoice = Invoice::findOrFail($invoice_id);

        $custid = $invoice->customer_id;

        $invoice->delete();

        return redirect('/customer/invoices/'.$custid)->with('status', 'Invoice deleted');
    }
    
    
    // Update an invoices items

    public function postUpdate(Request $request)
    {
        $invoice_data = json_decode($request->invoice, true);

        $invoice = Invoice::findOrFail($invoice_data['id']);

        // check for removals and update the already existing items.

        foreach ($invoice->items as $item) {
            $search = collect($invoice_data['items'])->filter(function ($filter) use ($item) {
                return $filter['id'] == $item->id;
            });

            if ($search->count() == 0) { // its been deleted
                $item->delete();
            } else {
                $updated_item = $search->pop();
                $item->update(['name' => $updated_item['name'], 'price' => $updated_item['price'], 'quantity' => $updated_item['quantity']]);
            }
        }

        // Check for new additions

        foreach ($invoice_data['items'] as $item) {

            if ($item['id'] == null || $item['id'] == null || !is_numeric($item['id'])) {
                $invoice->addItem($item['name'], $item['price'], $item['quantity']);
            }
        }
        
        // return the current invoice to update the interface
        return $invoice->fresh();
    }

    
    // Create a new invoice then return the invoice id for the front end to redirect to the edit page.
    public function postNew(Request $request)
    {

        $invoice_data = json_decode($request->invoice, true);
        $customer = Customer::findOrFail($invoice_data['customer_id']);

        $new_invoice = $customer->createInvoice();

        foreach ($invoice_data['items'] as $item) {
            $new_invoice->addItem($item['name'], $item['price'], $item['quantity']);
        }
        
        return ['id' => $new_invoice->id];

    }

}