<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 14/06/2017
 * Time: 19:25
 */

namespace App\Http\Controllers;


use App\BreadCrumbs;
use App\Customer;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('breadcrumbs');
       
    }


    public function getIndex()
    {
        // reset the breadcrumbs because we're at the dashboard
        breadcrumbs()->forget();
        breadcrumbs('index', 'Invoicing dashboard');
        return view('pages.index');
    }

    // return the manage customers page

    public function getCustomers()
    {
        breadcrumbs()->forget();
        breadcrumbs('index', 'Manage customers');
        return view('pages.search_customers');
    }
    

}