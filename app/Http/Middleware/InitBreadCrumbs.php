<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 15/06/2017
 * Time: 14:34
 */

namespace App\Http\Middleware;

use App\BreadCrumbs;
use Closure;

class InitBreadCrumbs
{


    public function handle( $request, Closure $next) {
        if (!session()->has('breadcrumbs')) {
            session()->put('breadcrumbs', new BreadCrumbs());
        }
        return $next($request);
    }
}